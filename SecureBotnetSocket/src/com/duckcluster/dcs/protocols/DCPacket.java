package com.duckcluster.dcs.protocols;

import java.io.Serializable;
import java.util.HashMap;

@SuppressWarnings("unchecked")
public class DCPacket implements Serializable
{
	private static final long											serialVersionUID	= -6119240487292699353L;

	private transient static Float										VERSION				= 1.1F;

	private transient static int										VERSION_FIELD		= 0;
	private transient static int										SECURED_FIELD		= 1;
	private transient static int										REQUEST_TYPE_FIELD	= 2;
	private transient static int										DATA_FIELD			= 3;

	private final HashMap<Integer, DataWrapper<? extends Serializable>>	fields				= new HashMap<>();

	/**
	 * @param <T>
	 *            Datatype of {@link #DATA_FIELD}
	 */
	public <T extends Serializable> DCPacket(final RequestType requestType, final T data)
	{
		fields.put(VERSION_FIELD, DataWrapper.wrap(VERSION));
		fields.put(REQUEST_TYPE_FIELD, DataWrapper.wrap(requestType));
		fields.put(SECURED_FIELD, DataWrapper.wrap(false));
		fields.put(DATA_FIELD, DataWrapper.wrap(data));
	}

	/**
	 * Returns the Version of the protocol.
	 * 
	 * @return {@link #VERSION}
	 */
	public Float getVersion()
	{
		return (Float) fields.get(VERSION_FIELD).get();
	}

	public Boolean isSecured()
	{
		return (Boolean) fields.get(SECURED_FIELD).get();
	}

	public RequestType getRequestType()
	{
		return (RequestType) fields.get(REQUEST_TYPE_FIELD).get();
	}

	/**
	 * Returns the data that is inside of the {@link #DATA_FIELD} of {@link #fields the HashMap}.
	 * 
	 * @return the value of {@link #DATA_FIELD}
	 */
	public <E extends Serializable> E getData()
	{
		return (E) fields.get(DATA_FIELD).get();
	}

}
