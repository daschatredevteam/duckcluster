package com.duckcluster.dcs.protocols;

public enum RequestType
{
	OS_INFO,
	COMMAND,
	INFO;
}
