package com.duckcluster.dcs.protocols;

import java.io.Serializable;

public class DataWrapper<T extends Serializable> implements Serializable
{
	private static final long	serialVersionUID	= 5943018783787011974L;

	private T					data;

	public T get()
	{
		return data;
	}

	public void set(final T data)
	{
		this.data = data;
	}

	public DataWrapper(final T data)
	{
		set(data);
	}

	public static <E extends Serializable> DataWrapper<E> wrap(final E data)
	{
		return new DataWrapper<>(data);
	}
}
