package com.duckcluster.dcs.sockets;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class DCServerSocket extends ServerSocket
{
	public DCServerSocket(final int port) throws IOException
	{
		super(port);
	}

	@Override
	public Socket accept() throws IOException
	{
		final Socket s = new DCSocket();
		implAccept(s);
		return s;
	}

}
