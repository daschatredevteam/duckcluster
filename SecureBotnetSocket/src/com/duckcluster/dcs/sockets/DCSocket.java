package com.duckcluster.dcs.sockets;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;

import com.duckcluster.dcs.protocols.DCPacket;
import com.duckcluster.dcs.protocols.RequestType;

public class DCSocket extends Socket
{
	private DataInputStream		in;
	private DataOutputStream	out;

	@Override
	public DataInputStream getInputStream() throws IOException
	{
		if (in == null)
		{
			in = new DataInputStream(super.getInputStream());
		}

		return in;
	}

	@Override
	public DataOutputStream getOutputStream() throws IOException
	{
		if (out == null)
		{
			out = new DataOutputStream(super.getOutputStream());
		}

		return out;
	}

	public DCSocket()
	{
		super();
	}

	public DCSocket(final String host, final int port) throws IOException
	{
		super(host, port);
	}

	public <T extends Serializable> void sendPacket(final RequestType requestType, final T data) throws IOException
	{
		final DCPacket packet = new DCPacket(requestType, data);

		try (final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
						final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);)
		{
			objectOutputStream.writeObject(packet);
			objectOutputStream.flush();

			final byte[] toSend = byteArrayOutputStream.toByteArray();

			getOutputStream().writeInt(toSend.length);
			getOutputStream().write(toSend);
		}
	}

	public DCPacket receivePacket()
	{
		try
		{
			final byte[] data = new byte[getInputStream().readInt()];
			getInputStream().read(data);

			try (final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
							final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);)
			{
				return (DCPacket) objectInputStream.readObject();
			}

		}
		catch (final IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final DCSocket other = (DCSocket) obj;
		if (!other.getInetAddress().getHostAddress().equals(getInetAddress().getHostAddress()))
		{
			return false;
		}
		return true;
	}

	@Override
	public int hashCode()
	{
		return getInetAddress().getHostAddress().hashCode();
	}

}
