package com.duckcluster.server.threads;

import java.util.Map;
import java.util.logging.Level;

import com.duckcluster.dcs.protocols.DCPacket;
import com.duckcluster.dcs.protocols.RequestType;
import com.duckcluster.server.main.Client;
import com.duckcluster.server.main.ClientManager;
import com.duckcluster.server.main.Server;

public class ListenerThread extends Thread
{
	private final Client client;

	public ListenerThread(final Client client)
	{
		this.client = client;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run()
	{
		try
		{
			final DCPacket packet = client.getSocket().receivePacket();
			if (packet.getRequestType() == RequestType.OS_INFO)
			{
				// We only send the os information once as a map
				ClientManager.setOSInfo(client, (Map<String, String>) packet.getData());
				Server.logger.log(Level.INFO, "Got " + client.toString() + "'s os information");
			}
		}
		catch (final Exception e)
		{
			// Don't give a fuck
		}

	}
}
