package com.duckcluster.server.main;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.duckcluster.dcs.protocols.RequestType;
import com.duckcluster.dcs.sockets.DCServerSocket;
import com.duckcluster.dcs.sockets.DCSocket;
import com.duckcluster.server.threads.ListenerThread;

public class ClientManager
{
	private final Logger									logger		= Logger.getLogger(this.getClass().getName());

	private static ClientManager							instance	= null;

	private final static List<Client>						clients		= new ArrayList<>();

	private final static Map<Client, Map<String, String>>	osInfo		= new HashMap<>();

	private static int										SERVER_PORT	= 1099;

	private static Thread									connectionListener;

	public ClientManager()
	{
		// Using port 1099, opened for RMI (RemoteMethodInvocation) bust mostly not used.
		connectionListener = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				logger.log(Level.INFO, "Creating ServerSocket.");
				try (final DCServerSocket serverSocket = new DCServerSocket(SERVER_PORT);)
				{
					// Wir warten bis wir eine eingehende Verbindung bekommen
					logger.log(Level.INFO, "Waiting for Clients to connect.");
					while (true)
					{
						try
						{
							final DCSocket clientSocket = (DCSocket) serverSocket.accept();
							final Client client = new Client(clientSocket);
							clients.remove(client);
							if (clients.add(client))
							{
								logger.log(Level.INFO, "Client " + client + " connected");
							}
							final ListenerThread clientOSInfoListener = new ListenerThread(client);
							clientOSInfoListener.start();
							final long startTime = System.currentTimeMillis();
							while (clientOSInfoListener.isAlive() && System.currentTimeMillis() < startTime + 1000)
							{
								// Wait maximum 1 sec for the os information
							}
							clientOSInfoListener.interrupt();
						}
						catch (final IOException errorAcceptClientConnection)
						{
							logger.log(Level.SEVERE, "Couldn't accept clientconnection.", errorAcceptClientConnection);
						}
					}
				}
				catch (final IOException errorCreatingSocket)
				{
					logger.log(Level.SEVERE, "Error creating ServerSocket.", errorCreatingSocket);
				}
			}
		});
		connectionListener.start();
	}

	public static ClientManager getInstance()
	{
		if (instance == null)
		{
			instance = new ClientManager();
		}
		return instance;
	}

	public void sendPacket(final Client receiver, final RequestType requestType, final String data)
	{
		try
		{
			receiver.getSocket().sendPacket(requestType, data);
		}
		catch (final IOException e)
		{
			logger.log(Level.INFO, "Lost client " + receiver + " (" + e.getMessage() + ")");
			clients.remove(receiver);
		}
	}

	public void sendCommandToAllClients(final String command)
	{
		logger.log(Level.INFO,
						"Sending Command:" + System.lineSeparator() + command + System.lineSeparator() + "to all (" + clients.size() + ") clients.");

		final List<Client> clonedClients = new ArrayList<>();
		clonedClients.addAll(clients);
		for (final Client client : clonedClients)
		{
			sendPacket(client, RequestType.COMMAND, command);
		}
	}

	public List<Client> getClients()
	{
		return clients;
	}

	public String getClientOSInfo(final Client client, final String key)
	{
		final Map<String, String> clientOSInfo = osInfo.get(client);
		if (clientOSInfo.containsKey(key))
		{
			return clientOSInfo.get(key);
		}
		return "Not available";
	}

	public void cancelListening()
	{
		connectionListener.interrupt();
		for (final Client connectedClient : clients)
		{
			try
			{
				connectedClient.getSocket().close();
			}
			catch (final IOException e)
			{
			}
		}
	}

	public static void setOSInfo(final Client client, final Map<String, String> data)
	{
		osInfo.put(client, data);
	}

	public Client getClientByAddress(final InetAddress ipAddress)
	{
		for (final Client client : clients)
		{
			if (client.getSocket().getInetAddress().getHostAddress().equals(ipAddress.getHostName()))
			{
				return client;
			}
		}
		return null;
	}
}
