package com.duckcluster.server.main;

import com.duckcluster.dcs.sockets.DCSocket;

public class Client
{
	private final DCSocket socket;

	public Client(final DCSocket socket)
	{
		this.socket = socket;
	}

	public DCSocket getSocket()
	{
		return socket;
	}

	@Override
	public String toString()
	{
		return socket.getInetAddress().getHostAddress().toString();
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final Client other = (Client) obj;
		if (socket == null)
		{
			if (other.socket != null)
			{
				return false;
			}
		}
		else if (!socket.equals(other.socket))
		{
			return false;
		}
		return true;
	}

	@Override
	public int hashCode()
	{
		return socket.hashCode();
	}

}
