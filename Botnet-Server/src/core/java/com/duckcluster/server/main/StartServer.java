package com.duckcluster.server.main;

public class StartServer
{
	public static void main(final String[] args)
	{
		final Server server = new Server();
		server.start();
	}
}
