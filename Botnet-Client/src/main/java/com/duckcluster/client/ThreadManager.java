package com.duckcluster.client;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages the threads of the duck.
 * 
 * @since 27.04.2017
 */
public class ThreadManager
{
	/** Maximum allowed number of {@link ParentThread ParentThreads}. */
	private int								allowedNumberOfParentThreads	= 1;

	/** Map that holds all {@link ParentThread ParentThreads} with their names as value. */
	private final Map<ParentThread, String>	parentThreads					= new HashMap<>();

	/** Singleton instance */
	private static ThreadManager			instance;

	/**
	 * @return the singleton instance of {@link ThreadManager}
	 */
	public static ThreadManager getInstance()
	{
		if (instance == null)
		{
			instance = new ThreadManager();
		}

		return instance;
	}

	/**
	 * Private constructor, so no instances can be created.
	 */
	private ThreadManager()
	{
	}

	/**
	 * Sets the number of maximum parallel {@link ParentThread ParentThreads}.
	 * 
	 * @param maxThreads
	 *            new number of allowed threads
	 */
	public void setNumberOfMaxAllowedParentThreads(final int maxThreads)
	{
		allowedNumberOfParentThreads = maxThreads;
	}

	/**
	 * Registers a new {@link ParentThread}.
	 * 
	 * @param thread
	 *            the {@link ParentThread} to be registered
	 * @param threadName
	 *            the name to register the {@link ParentThread} by
	 * @return true if the registration was successful
	 */
	public boolean registerNewParentThread(final ParentThread thread, final String threadName)
	{
		if (parentThreads.size() >= allowedNumberOfParentThreads)
		{ // Max number of threads has already been reached
			return false;
		}

		parentThreads.put(thread, threadName);
		return true;
	}

	/**
	 * Adds a children Thread to a {@link ParentThread}, searching the {@link ParentThread} by its name.
	 * 
	 * @param parentThreadName
	 *            the name to search the parent thread by
	 * @param newChildThread
	 *            the children thread, that will be added to the {@link ParentThread}
	 */
	public void addChildrenToParent(final String parentThreadName, final Thread newChildThread)
	{
		final ParentThread parent = getParentThreadByName(parentThreadName);
		if (parent != null)
		{
			addChildrenToParent(parent, newChildThread);
		}
	}

	/**
	 * Adds a children Thread to a {@link ParentThread}.
	 * 
	 * @param parentThread
	 *            the {@link ParentThread} to add the children to
	 * @param newChildThread
	 *            the children thread, that will be added to the {@link ParentThread}
	 */
	public void addChildrenToParent(final ParentThread parentThread, final Thread newChildThread)
	{
		if (!parentThreads.containsKey(parentThread))
		{
			throw new IllegalArgumentException(
							"The thread, that you are trying to add a children to, is not managed by this ThreadManager instance.");
		}

		parentThread.addChildren(newChildThread);
	}

	/**
	 * Executes a {@link ParentThread} searching it by its name.
	 * 
	 * @param parentThreadName
	 *            the name to search the {@link ParentThread} by
	 */
	public void executeParentThread(final String parentThreadName)
	{
		final ParentThread parent = getParentThreadByName(parentThreadName);
		if (parent != null)
		{
			executeParentThread(parent);
		}
	}

	/**
	 * Executes a {@link ParentThread}.
	 * 
	 * @param parentThread
	 *            the {@link ParentThread} to execute
	 */
	public void executeParentThread(final Thread parentThread)
	{
		if (!parentThreads.containsKey(parentThread))
		{
			throw new IllegalArgumentException("The thread, that you are trying to execute, is not managed by this ThreadManager instance.");
		}
		if (parentThread.isAlive())
		{
			throw new IllegalArgumentException("The thread, that you are trying to execute, is already running.");
		}

		parentThread.start();
	}

	/*
	 * +
	 * Collapses all {@link ParentThread ParentThreads} and its children.
	 */
	public void collapseAllThreads()
	{
		for (final ParentThread parentThread : parentThreads.keySet())
		{
			parentThread.interrupt();
		}
	}

	/**
	 * Collapses a {@link ParentThread} and all his children, searching it by its name.
	 * 
	 * @param parentThreadName
	 *            the name to search by
	 * @return true (might change later on)
	 */
	public boolean collapseParentThread(final String parentThreadName)
	{
		final ParentThread parent = getParentThreadByName(parentThreadName);
		return parent == null ? false : collapseParentThread(parent);
	}

	/**
	 * Collapses a {@link ParentThread} and all his children.
	 * 
	 * @param parentThread
	 *            the {@link ParentThread} to collapse
	 * @return true (might change later on)
	 */
	public boolean collapseParentThread(final Thread parentThread)
	{
		if (!parentThreads.containsKey(parentThread))
		{
			throw new IllegalArgumentException("The thread, that you are trying to collapse, is not managed by this ThreadManager instance.");
		}
		if (!parentThread.isAlive())
		{
			// TODO(MSC) Should we still try to collapse the children threads,
			return true; // True, because the thread is already interrupted.
		}

		// TODO(MSC) Do we have to expect a crash under "normal" circumstances?
		parentThread.interrupt();
		parentThreads.remove(parentThread);

		return true;
	}

	/**
	 * Reports, whether a {@link ParentThread} is alive, searching it by its name.
	 * 
	 * @param parentThreadName
	 *            the name to search by
	 * @return true if alive, false otherwise
	 */
	public boolean isParentThreadAlive(final String parentThreadName)
	{
		final ParentThread parent = getParentThreadByName(parentThreadName);
		return parent == null ? false : parent.isAlive();
	}

	/**
	 * Returns a{@link ParentThread}, searching it by its name.
	 * 
	 * @param parentThreadName
	 *            the name to search by
	 * @return the {@link ParentThread} that has been found or <code>null</code>
	 */
	public ParentThread getParentThreadByName(final String parentThreadName)
	{
		for (final ParentThread parent : parentThreads.keySet())
		{
			if (parentThreads.get(parent).equals(parentThreadName))
			{
				return parent;
			}
		}

		return null;
	}

}
