package com.duckcluster.client.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import com.duckcluster.client.Client;
import com.duckcluster.dcs.protocols.RequestType;

public class OSUtil
{
	private final static String OS_NAME = System.getProperty("os.name");

	public static String getOSName()
	{
		return OS_NAME;
	}

	public static boolean isWindows()
	{
		return OS_NAME.toLowerCase().contains("win");
	}

	public static String getCountry()
	{
		return System.getProperty("user.country");
	}

	public static String getUserName()
	{
		return System.getProperty("user.name");
	}

	public static String getArchitecture()
	{
		return System.getProperty("sun.arch.data.model");
	}

	public static String getLanguage()
	{
		return System.getProperty("user.language");
	}

	public static Integer getCores()
	{
		return Runtime.getRuntime().availableProcessors();
	}

	public static String getCPU()
	{
		final String cpuName = "";
		if (isWindows())
		{
			final ArrayList<String> fileContent = StringUtil.fileContentToString("/proc/cpuinfo");
			for (final String line : fileContent)
			{
				if (line.startsWith("model name"))
				{
					final String[] lines = line.split(":");
					return lines[1].trim();
				}
			}
		}
		else
		{
			final ArrayList<String> output = runCommand("wmic", "cpu", "get", "Name");
			if (output.size() > 1)
			{
				return output.get(1).trim();
			}
		}
		return cpuName;
	}

	public static String getMaxRAM()
	{
		final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		for (final Method method : operatingSystemMXBean.getClass().getDeclaredMethods())
		{
			method.setAccessible(true);
			if (Modifier.isPublic(method.getModifiers()))
			{
				try
				{
					if (method.getName().equalsIgnoreCase("getTotalPhysicalMemorySize"))
					{
						return Sizeunit.convertBytesizeToString(Long.parseLong("" + method.invoke(operatingSystemMXBean)));
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static String getFreeRAM()
	{
		final OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		for (final Method method : operatingSystemMXBean.getClass().getDeclaredMethods())
		{
			method.setAccessible(true);
			if (Modifier.isPublic(method.getModifiers()))
			{
				try
				{
					if (method.getName().equalsIgnoreCase("getFreePhysicalMemorySize"))
					{
						return Sizeunit.convertBytesizeToString(Long.parseLong("" + method.invoke(operatingSystemMXBean)));
					}
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public static String getUsedRAM()
	{
		return Sizeunit.convertBytesizeToString(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
	}

	@Deprecated
	public static String getCpuUsage()
	{
		AttributeList list = null;
		try
		{
			final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
			final ObjectName name = ObjectName.getInstance("java.lang:type=OperatingSystem");
			list = mbs.getAttributes(name, new String[] { "SystemCpuLoad" });
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}

		if (!list.isEmpty())
		{
			final Attribute att = (Attribute) list.get(0);
			final Double value = (Double) att.getValue();
			if (value != -1.0)
			{
				return (int) (value * 1000) / 10.0 + " %";
			}
		}
		return null;
	}

	public static String getComputerName()
	{
		try
		{
			return InetAddress.getLocalHost().getHostName();
		}
		catch (final UnknownHostException e)
		{
			System.err.println("Unable to get the computers name!");
			e.printStackTrace();
		}
		return null;
	}

	public static ArrayList<String> getUserGroups()
	{

		final ArrayList<String> command = new ArrayList<>();
		if (isWindows())
		{
			command.add("net");
			command.add("user");
			command.add("\"" + getUserName() + "\"");
			final ArrayList<String> output = runCommand(command);
			final ArrayList<String> retunValue = new ArrayList<>();
			for (String str : output)
			{
				if (!str.contains("*"))
				{
					continue;
				}
				if (str.contains("*None"))
				{
					continue;
				}
				str = str.replace("Lokale Gruppenmitgliedschaften", "");
				str = str.replace("Globale Gruppenmitgliedschaften", "");
				str = str.replace("*", "");
				str = str.trim();
				retunValue.add(str);
			}
			System.out.println("Usergroups: " + retunValue);
			return retunValue;
		}
		command.add("groups");
		command.add(getUserName());
		final ArrayList<String> output = runCommand(command);

		if (output.size() == 1)
		{
			final ArrayList<String> groups = new ArrayList<>();
			for (final String group : output.get(0).replace(getUserName() + " : ", "").split(" "))
			{
				groups.add(group);
			}
			return groups;
		}
		return null;
	}

	public static ArrayList<String> getUsers()
	{
		final ArrayList<String> command = new ArrayList<>();
		if (isWindows())
		{
			command.add("net");
			command.add("user");
			final ArrayList<String> output = runCommand(command);

			final ArrayList<String> users = new ArrayList<>();
			for (final String user : output)
			{
				if (user.contains("----") || user.contains("Benutzerkonten") || user.contains("Der Befehl"))
				{
					continue;
				}
				for (String str : user.split("  "))
				{
					str = str.trim();
					if (!str.equalsIgnoreCase(""))
					{
						System.out.println("Adding user: " + str);
						users.add(str);
					}
				}
			}
			System.out.println("Users: " + users);
			return users;
		}
		command.add("cut");
		command.add("-d:");
		command.add("-f1");
		command.add("/etc/passwd");
		final ArrayList<String> output = runCommand(command);

		final ArrayList<String> users = new ArrayList<>();
		for (final String user : output)
		{
			users.add(user);
		}
		return users;
	}

	public static void restartOS()
	{
		Client.getInstance().sendPacket(RequestType.INFO, "os restart");
		try
		{
			Runtime.getRuntime().exec("sudo shutdown -r now");
		}
		catch (final Exception e)
		{
			Client.getInstance().sendPacket(RequestType.INFO, "os restart failed");
		}
	}

	public static void stopOS()
	{
		Client.getInstance().sendPacket(RequestType.INFO, "os shutdown");
		try
		{
			Runtime.getRuntime().exec("sudo shutdown now");
		}
		catch (final Exception e)
		{
			Client.getInstance().sendPacket(RequestType.INFO, "os shutdown failed");
		}
	}

	public static ArrayList<String> runCommand(final String... commands)
	{
		final ArrayList<String> commandList = new ArrayList<>();
		for (final String command : commands)
		{
			commandList.add(command);
		}
		return runCommand(commandList);
	}

	public static ArrayList<String> runCommand(final ArrayList<String> command)
	{
		final ArrayList<String> output = new ArrayList<>();
		try
		{
			String s = null;
			final ProcessBuilder pb = new ProcessBuilder(command);
			final Process process = pb.start();
			final BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
			final BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

			// get output
			while ((s = stdInput.readLine()) != null)
			{
				output.add(s);
			}

			// get errors
			while ((s = stdError.readLine()) != null)
			{
				output.add(s);
			}
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}

		if (Client.isDebug())
		{
			System.out.println("Output of the command '" + StringUtil.listToString(command, " ") + "':");
			for (final String entry : output)
			{
				System.out.println(entry);
			}
		}
		return output;
	}
}
