package com.duckcluster.client.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.duckcluster.client.Client;

public class StringUtil
{
	private static final Logger logger = Logger.getLogger(Client.class.getName());

	public static String listToString(final ArrayList<String> list, final String spacer)
	{
		return listToString(list, spacer, spacer);
	}

	public static String listToString(final ArrayList<String> list, final String spacer, final String lastspacer)
	{
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < list.size(); i++)
		{
			if (i == list.size() - 1)
			{
				sb.append(list.get(i));
			}
			else if (i == list.size() - 2)
			{
				sb.append(list.get(i) + lastspacer);
			}
			else
			{
				sb.append(list.get(i) + spacer);
			}
		}
		return sb.toString();
	}

	public static String getArgumentsToString(final String[] args, final int num)
	{
		final StringBuilder sb = new StringBuilder();
		for (int i = num; i < args.length; ++i)
		{
			sb.append(args[i]).append(" ");
		}
		return sb.toString().trim();
	}

	public static String escapeString(final String statementArgument)
	{
		if (statementArgument == null)
		{
			return null;
		}
		return statementArgument.replace("'", "").replace("´", "").replace("`", "").replace(";", "").replace("#", "").replace("--", "")
						.replace("/*", "").replace("*/", "");
	}

	public static ArrayList<String> fileContentToString(final String filename)
	{
		return fileContentToString(new File(filename));
	}

	public static ArrayList<String> fileContentToString(final File file)
	{
		if (!file.exists())
		{
			return new ArrayList<>();
		}
		final ArrayList<String> content = new ArrayList<>();
		BufferedReader br = null;
		FileReader fr = null;
		try
		{
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String sCurrentLine;
			br = new BufferedReader(new FileReader(file));
			while ((sCurrentLine = br.readLine()) != null)
			{
				content.add(sCurrentLine);
			}
		}
		catch (final IOException e)
		{
			logger.log(Level.SEVERE, "Unable to read file " + file.getAbsolutePath(), e);
		}
		finally
		{
			try
			{
				if (br != null)
				{
					br.close();
				}
				if (fr != null)
				{
					fr.close();
				}
			}
			catch (final Exception ex)
			{
				// Gibt schlimmeres, als dass ein Reader nicht geschlossen werden kann
			}
		}
		return content;
	}

	public static String listToFileString(final ArrayList<String> content, final boolean removeEmptyLines)
	{
		final StringBuilder sb = new StringBuilder();
		for (final String line : content)
		{
			sb.append(line + "\r\n");
		}
		return sb.toString();
	}
}
