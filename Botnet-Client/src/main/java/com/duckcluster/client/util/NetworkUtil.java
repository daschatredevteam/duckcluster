package com.duckcluster.client.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.UnknownHostException;

public class NetworkUtil
{
	public static String getRemoteAddress()
	{
		try
		{
			final URL amazonIpChecker = new URL("http://checkip.amazonaws.com");
			final BufferedReader inputReader = new BufferedReader(new InputStreamReader(amazonIpChecker.openStream()));
			return inputReader.readLine();
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			System.err.println("Unable to get remote address!");
		}
		return null;
	}

	public static String getLocalAddress()
	{
		try
		{
			return InetAddress.getLocalHost().getHostAddress();
		}
		catch (final UnknownHostException e)
		{
			e.printStackTrace();
			System.err.println("Unable to get local address!");
		}
		return null;
	}

	@Deprecated
	public static String getMACAddress()
	{
		try
		{
			final byte[] b = NetworkInterface.getByInetAddress(InetAddress.getLocalHost()).getHardwareAddress();
			final StringBuilder sb = new StringBuilder();
			for (int i = 0; i < b.length; i++)
			{
				sb.append(String.format("%02X%s", b[i], i < b.length - 1 ? "-" : ""));
			}
			return sb.toString();
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			System.err.println("Unable to get own MAC Address");
		}
		return null;
	}

}
