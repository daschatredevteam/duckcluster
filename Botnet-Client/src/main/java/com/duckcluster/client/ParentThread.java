package com.duckcluster.client;

import java.util.ArrayList;
import java.util.List;

public class ParentThread extends Thread
{
	private final List<Thread> children = new ArrayList<>();

	@Override
	public void interrupt()
	{
		for (final Thread child : children)
		{
			child.interrupt();
		}
		children.clear();
		super.interrupt();
	}

	public void addChildren(final Thread newChild)
	{
		children.add(newChild);
	}
}
