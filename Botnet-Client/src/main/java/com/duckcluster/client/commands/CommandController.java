package com.duckcluster.client.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class CommandController
{
	private static final Logger logger = Logger.getLogger(CommandController.class.getName());

	/**
	 * Processes the given command.
	 * 
	 * @param givenInput
	 *            command input to process.
	 */
	public static void processCommand(final String givenInput)
	{
		final List<String> parametersSource = Arrays.asList(givenInput.split(" "));
		try
		{
			final Command command = Commands.getByName(parametersSource.get(0)).getCommandExecuteable();
			final Map<String, String> target = new HashMap<>();
			if (parametersSource.size() > 1)
			{
				target.putAll(parseParameters(parametersSource.subList(1, parametersSource.size())));
			}
			command.execute(target);
		}
		catch (final UnknownCommandException unknownCommand)
		{
			logger.severe("Error executing command:" + unknownCommand.getMessage());
		}
	}

	private static Map<String, String> parseParameters(final List<String> source)
	{
		// System.out.println("parsing: " + source.toString());
		final Map<String, String> target = new HashMap<>();
		for (int i = 0; i < source.size() - 1; i++)
		{
			// System.out.println(i + ". = " + source.get(i));
			final String paramName = source.get(i);
			String subParameter = null;
			if (source.size() >= i + 2)
			{
				// System.out.println("param: " + source.get(i + 1));
				subParameter = source.get(i + 1);
				if (!subParameter.startsWith("-"))
				{
					// System.out.println("startswith -");
					i++;
					while (true)
					{
						if (subParameter.startsWith("\"") && !subParameter.endsWith("\""))
						{
							i += 1;
							subParameter += " " + source.get(i);
						}
						else
						{
							break;
						}
					}
				}
			}
			target.put(paramName, subParameter);
		}

		return target;
	}
}
