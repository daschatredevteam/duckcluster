package com.duckcluster.client.commands.executeables;

import java.util.Map;

import com.duckcluster.client.ThreadManager;
import com.duckcluster.client.commands.Command;

public class CommandStopAttack implements Command
{
	@SuppressWarnings("unused")
	@Override
	public void execute(final Map<String, String> parameters)
	{
		ThreadManager.getInstance().collapseAllThreads();
	}
}
