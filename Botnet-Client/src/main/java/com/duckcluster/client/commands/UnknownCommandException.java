package com.duckcluster.client.commands;

/**
 * Thrown, if a Command is not known by the Client.
 */
public class UnknownCommandException extends RuntimeException
{
	/**
	 * Default generated SerialVersionID.
	 */
	private static final long serialVersionUID = -6601580646970509900L;

	public UnknownCommandException(final String msg)
	{
		super(msg);
	}
}
