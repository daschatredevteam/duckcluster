package com.duckcluster.client.commands.executeables;

import java.util.Map;

import com.duckcluster.client.Client;
import com.duckcluster.client.commands.Command;

public class CommandRestartDuck implements Command
{
	@SuppressWarnings("unused")
	@Override
	public void execute(final Map<String, String> parameters)
	{
		Client.getInstance().restartDuck();
	}
}
