package com.duckcluster.client.commands.executeables;

import java.util.ArrayList;
import java.util.Map;

import com.duckcluster.client.commands.Command;
import com.duckcluster.client.util.OSUtil;

public class CommandShellExecution implements Command
{
	@Override
	public void execute(final Map<String, String> parameters)
	{
		final ArrayList<String> commandList = new ArrayList<>();
		for (final String s : parameters.keySet())
		{
			commandList.add(s);
			commandList.add(parameters.get(s));
		}
		OSUtil.runCommand(commandList);
	}
}
