package com.duckcluster.client.commands.executeables;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.duckcluster.client.Client;
import com.duckcluster.client.commands.Command;
import com.duckcluster.shared.util.FileUtility;

public class CommandUpdate implements Command
{
	private static String		updateURL	= "https://klar.ddns.net/download/duckcluster/apt.jar";

	private final static Logger	logger		= Logger.getLogger(CommandUpdate.class.getName());

	@SuppressWarnings("unused")
	@Override
	public void execute(final Map<String, String> parameters)
	{
		final Client clientInstance = Client.getInstance();

		logger.log(Level.INFO, "Updating client from: " + updateURL);

		try
		{
			final File testFile = new File(FileUtility.getOwnJarFile() + ".test");
			FileOutputStream fos = new FileOutputStream(testFile);
			fos.getChannel().transferFrom(Channels.newChannel(new URL(updateURL).openStream()), 0, Long.MAX_VALUE);
			fos.close();
			testFile.delete();

			fos = new FileOutputStream(FileUtility.getOwnJarFile());
			fos.getChannel().transferFrom(Channels.newChannel(new URL(updateURL).openStream()), 0, Long.MAX_VALUE);
			fos.close();
			clientInstance.restartDuck();
		}
		catch (final Exception updateFailed)
		{
			logger.log(Level.SEVERE, "Client couldn't be updated.", updateFailed);
		}
	}

	public static void setUpdateURL(final String updateURL)
	{
		CommandUpdate.updateURL = updateURL;
		logger.log(Level.INFO, "Got new updateurl: " + updateURL);
	}

	public static String getUpdateUrl()
	{
		return updateURL;
	}
}
