package com.duckcluster.client.commands.executeables;

import java.util.Map;

import com.duckcluster.client.Client;
import com.duckcluster.client.commands.Command;
import com.duckcluster.dcs.protocols.RequestType;

public class CommandGetAlive implements Command
{
	@SuppressWarnings("unused")
	@Override
	public void execute(final Map<String, String> parameters)
	{
		Client.getInstance().sendPacket(RequestType.INFO, "alive");
	}
}
