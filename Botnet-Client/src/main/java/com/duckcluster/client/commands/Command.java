package com.duckcluster.client.commands;

import java.util.Map;

/**
 * Defines what a Command has to look like.
 */
public interface Command
{
	/**
	 * Executes the command using the given parameters.
	 * 
	 * @param parameters
	 *            given parameters
	 */
	public void execute(Map<String, String> parameters);
}
