package com.duckcluster.client.commands.executeables;

import java.util.Map;

import com.duckcluster.client.commands.Command;
import com.duckcluster.client.util.OSUtil;

public class CommandRestart implements Command
{
	@SuppressWarnings("unused")
	@Override
	public void execute(final Map<String, String> parameters)
	{
		OSUtil.restartOS();
	}
}