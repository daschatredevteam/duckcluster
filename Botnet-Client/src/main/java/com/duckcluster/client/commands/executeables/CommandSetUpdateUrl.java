package com.duckcluster.client.commands.executeables;

import java.util.Map;

import com.duckcluster.client.commands.Command;

public class CommandSetUpdateUrl implements Command
{
	@Override
	public void execute(final Map<String, String> parameters)
	{
		final String updateURL = parameters.get("-url");
		CommandUpdate.setUpdateURL(updateURL);
	}
}
