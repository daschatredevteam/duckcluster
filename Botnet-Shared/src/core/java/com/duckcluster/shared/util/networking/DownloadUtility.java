package com.duckcluster.shared.util.networking;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

import com.duckcluster.shared.util.ArrayUtility;

public class DownloadUtility
{
	/**
	 * Downloads a file and saves it.
	 * 
	 * @param url
	 *            the url to download the content from
	 * @param output
	 *            the file that will be saved
	 * @return the downloaded file as a byte array
	 */
	public static void downloadFileAndSave(final String url, final File output) throws IOException
	{
		downloadFileAndSave(url, output, 0);
	}

	/**
	 * Downloads a file, saves it and optionally lets you limit the downloadspeed.
	 * 
	 * @param url
	 *            the url to download thé content from
	 * @param output
	 *            the file that will be saved
	 * @param downloadMaxInKiB
	 *            Maximum download speed per second
	 * @return the downloaded file as a byte array
	 */
	public static void downloadFileAndSave(final String url, final File output, final int downloadMaxInKiB) throws IOException
	{
		final byte[] fileAsBytes = downloadFileAsByteArray(url, downloadMaxInKiB);
		try (final FileOutputStream outputStream = new FileOutputStream(output))
		{
			outputStream.write(fileAsBytes);
		}
	}

	/**
	 * Downloads a file and returns it as an byte array.
	 * 
	 * @param url
	 *            the url to download thé content from
	 * @return the downloaded file as a byte array
	 */
	public static byte[] downloadFileAsByteArray(final String url) throws IOException
	{
		return downloadFileAsByteArray(url, 0);
	}

	/**
	 * Downloads a file and returns it as an byte array and optionally lets you limit the downloadspeed.
	 * 
	 * @param url
	 *            the url to download thé content from
	 * @param downloadMaxInKiB
	 *            Maximum download speed per second
	 * @return the downloaded file as a byte array
	 */
	public static byte[] downloadFileAsByteArray(final String url, final int downloadMaxInKiB) throws IOException
	{
		final DataInputStream input = new DataInputStream(new BufferedInputStream(new URL(url).openStream()));

		int maxSize = 1;

		byte[] data = new byte[0];
		final int chunkSize = 1024 * (downloadMaxInKiB > 0 ? downloadMaxInKiB : 1);
		byte[] temp = new byte[chunkSize];

		while (true)
		{
			final long startTime = System.currentTimeMillis();

			final int size = input.read(temp);

			maxSize += size;

			if (size == -1)
			{
				break;
			}

			data = ArrayUtility.joinArrays(data, temp);
			temp = new byte[chunkSize];

			if (downloadMaxInKiB > 0)
			{
				final long sleepTime = (startTime + 1000) - System.currentTimeMillis();
				if (sleepTime > 0)
				{
					try
					{
						Thread.sleep(sleepTime);
					}
					catch (final InterruptedException e)
					{
						System.out.println("Error sleeping");
						// TODO(MSC) Should something be done here?
					}
				}
			}

		}

		data = Arrays.copyOfRange(data, 0, maxSize);

		return data;
	}
}
