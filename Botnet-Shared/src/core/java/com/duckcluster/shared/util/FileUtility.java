package com.duckcluster.shared.util;

import java.io.File;
import java.io.FileNotFoundException;

public class FileUtility
{
	/**
	 * Return the applications jar file.
	 * 
	 * @return a File pointing to the applications own jar file
	 * @throws FileNotFoundException
	 *             if the jar file doesn't exist
	 */
	public static File getOwnJarFile()
	{
		return new File(System.getProperty("java.class.path")).getAbsoluteFile();
	}
}
