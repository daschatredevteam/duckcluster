package com.duckcluster.shared.util;

public class ObjectUtil
{
	public <T> T orElse(final T object, final T alternativeObject)
	{
		return object == null ? alternativeObject : object;
	}
}
