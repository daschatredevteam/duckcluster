package com.duckcluster.shared.util;

public class ArrayUtility
{
	/**
	 * Joins two arrays and returns the joined array.
	 * 
	 * @param a
	 *            Array one
	 * @param b
	 *            Array two
	 * @return joined array;
	 */
	public static byte[] joinArrays(final byte[] a, final byte[] b)
	{
		final byte[] destination = new byte[a.length + b.length];
		System.arraycopy(a, 0, destination, 0, a.length);
		System.arraycopy(b, 0, destination, a.length, b.length);
		return destination;
	}
}
